import { Component, OnInit, Inject } from '@angular/core';
import { navItems } from './_nav';
import { DOCUMENT } from '@angular/common';
import { AuthService } from '../core/services/auth.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  public navItems = navItems;
  public sidebarMinimized = true;
  private changes: MutationObserver;
  public element: HTMLElement;
  public userDetails : any ;
  constructor(
    private authService : AuthService,
    @Inject(DOCUMENT) _document?: any
    
  ) {
    this.getUserDetails();

    this.changes = new MutationObserver((mutations) => {
      this.sidebarMinimized = _document.body.classList.contains('sidebar-minimized');
    });
    this.element = _document.body;
    this.changes.observe(<Element>this.element, {
      attributes: true,
      attributeFilter: ['class']
    });
  }

  ngOnDestroy(): void {
    this.changes.disconnect();
  }

  getUserDetails() {
    let userData = localStorage.getItem('currentUser');
    if (userData) {
      let currentUser = JSON.parse(userData);
        this.userDetails = currentUser;
    }
  }

    /**
   * Logout
   */
  logout() {
    this.authService.logout();
  }


  ngOnInit() {
  }

}
