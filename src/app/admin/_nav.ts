interface NavAttributes {
    [propName: string]: any;
  }
  interface NavWrapper {
    attributes: NavAttributes;
    element: string;
  }
  interface NavBadge {
    text: string;
    variant: string;
  }
  interface NavLabel {
    class?: string;
    variant: string;
  }
  
  export interface NavData {
    name?: string;
    url?: string;
    icon?: string;
    badge?: NavBadge;
    title?: boolean;
    children?: NavData[];
    variant?: string;
    attributes?: NavAttributes;
    divider?: boolean;
    class?: string;
    label?: NavLabel;
    wrapper?: NavWrapper;
  }
  
  export const navItems: NavData[] = [
    // {
    //   name: 'Category',
    //   url: '/admin/category',
    //   icon: 'fa fa-tachometer',
    // },
    // {
    //   name: 'Product',
    //   url: '/admin/product',
    //   icon: 'fa fa-filter',
    // },
    {
      name: 'User',
      url: '/admin/user',
      icon: 'fa fa-user',
    },
    // {
    //   name: 'Role',
    //   url: '/admin/role-management',
    //   icon: 'fa fa-cogs',
    // },
  ];
  