import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { UsersComponent } from './users/users.component';

const routes: Routes = [
  {path : '', component : AdminComponent,
  children: [
    { path: 'user', component: UsersComponent, data: { title: 'Users' } },
    { path: '', redirectTo: 'user' , pathMatch: 'full'},
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
