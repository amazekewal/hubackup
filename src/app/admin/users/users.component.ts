import { Component, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { AdminService } from 'src/app/core/services/admin.service';

export interface PeriodicElement {
  id: any;
  firstName: any;
  email: any;
  region: any;
  payment: any;
  action: any;
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})



export class UsersComponent implements OnInit {
  @ViewChild('addUserModal', {static: true})  largeModal: ModalDirective;// Lab Model
  @ViewChild('smallModal', {static: true})  smallModal: ModalDirective;

  dataSource = new MatTableDataSource<PeriodicElement>();
  displayedColumns: string[] = ['id', 'firstName', 'email', 'region', 'payment', 'action'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  public user_id;
  public nodata;
  public loading = false;
  public modeltitle = 'Add User';
  public userDetails = '';

  constructor(
    private Adminservice: AdminService,
  ) {
    this.getUserList();
  }

  getUserById(data) {
    this.modeltitle = "User Details";
    this.Adminservice.getUserDetail(data).subscribe((res : any) =>{
      if(res){
        this.userDetails = res.user;
      }
    });
  }

  getUserId(id) {
    this.user_id = id;
  }

  deleteUser(id) {
    let data = {
      id: id
    }
    this.Adminservice.deleteUser(data).subscribe((res: any) => {
     if(res){
       this.smallModal.hide();
       this.getUserList();
     }
    });
  }

  getUserList() {
    this.loading = true;
    this.Adminservice.getUserList().subscribe((res: any) => {
      if (res) {
        // console.log(res.users);
        this.dataSource.data = res.users;
        this.dataSource.paginator = this.paginator;
        this.loading = false;
      }
    });
  }

  ngOnInit() {
  }

}
