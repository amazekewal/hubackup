import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-main-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  public currentUser;
  isLoggedIn$: Observable<boolean>;
  constructor(private authService : AuthService ) { 
    
  }

  ngOnInit() {
    this.isLoggedIn$ = this.authService.isLoggedIn;
  }

}
