import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { User, SignupUser, ForgotUser, changePwd, overview, hubauthAcess } from '../../shared/models/user';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdminService {
  private authUrl: string;
  constructor(
    private router: Router,
    private http: HttpClient
  ) {
    this.authUrl = environment.APIURL;

  }



  /**
   * Hubspot Backup Detail API **************************************************************************
   * DashBoard Graph And Backup Slider
   */
  getUserList() {
    return this.http.get<boolean>(this.authUrl + '/admin/users')
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }

  deleteUser(data) {
    return this.http.post<boolean>(this.authUrl + '/admin/user/destroy', data)
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }

  getUserDetail(id) {
    return this.http.get<boolean>(this.authUrl + '/admin/user/' + id)
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }


  /**
 * Hubspot Backup History API ****************************************************************************
 */
  backuphistory(id) {
    return this.http.get<boolean>(this.authUrl + '/hubspot-backup-history/' + id)
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }


}
