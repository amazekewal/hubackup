import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { User, SignupUser, ForgotUser, changePwd, overview, hubauthAcess } from '../../shared/models/user';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private loggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public currentUser;
  public userId;
  private authUrl: string;


  notifyCount = new EventEmitter();
  paymentSuccess = new EventEmitter();
  upgradeHubackup = new EventEmitter();
  tokenExpire = new EventEmitter();
  tokenLogin = new EventEmitter();

  constructor(
    private router: Router,
    private http: HttpClient
  ) {
    this.authUrl = environment.APIURL;
    // Check if user loggedin
    this.currentUser = localStorage.getItem('currentUser');
    this.userId = JSON.parse(this.currentUser);
  }

  /** Notification Emitter */
  notifyData() {
    this.notifyCount.emit();
  }

  /**
   * On paymentSucessfull
   */

  onpayment() {
    this.paymentSuccess.emit();
  }

  upgradeAccount() {
    this.upgradeHubackup.emit();
  }

  ontokenExpire() {
    this.tokenExpire.emit();
  }

  onUserlogin() {
    this.tokenLogin.emit();
  }



  get isLoggedIn() {
    if (this.currentUser) {
      this.loggedIn.next(true);
    }
    return this.loggedIn.asObservable();
  }

  /**
   * 
   * @param user 
   */
  login(user: User): Observable<any> {
    return this.http.post<boolean>(this.authUrl + '/auth/login', user)
      .pipe(
        map((isLoggedIn: any) => {
          localStorage.setItem('token', JSON.stringify(isLoggedIn.token));
          localStorage.setItem('currentUser', JSON.stringify(isLoggedIn.user));
          this.loggedIn.next(true);
          return true;
        }),
        // catchError(this.handleError)
      );
  }

  /**
 * Event on user Signup
 * User Signup 
 * @param user 
 */
  signup(user: SignupUser) {
    return this.http.post<boolean>(this.authUrl + '/auth/signup', user)
      .pipe(
        map((isLoggedIn: any) => {
          return true;
        }),
      );
  }


  /**
   * Payment Subscriptions
   * @param user 
   */

  paymentToken(data) {
    return this.http.post<boolean>(this.authUrl + '/stripe', data)
      .pipe(
        map((isLoggedIn: any) => {
          return true;
        }),
      );
  }



  /**
   * Forget password
   */
  forgotPassword(user: ForgotUser) {
    return this.http.post<boolean>(this.authUrl + '/auth/forgot', user)
      .pipe(
        map((isSend: any) => {
          return true;
        }),
      );
  }

  /**
   * Change user password
   */
  changePassword(user: changePwd) {
    return this.http.post<boolean>(this.authUrl + '/auth/reset', user)
      .pipe(
        map((isChange: any) => {
          return true;
        }),
      );
  }

  /**
   * Update user
   */
  updateOverview(user: overview) {
    return this.http.post<boolean>(this.authUrl + '/auth/update', user)
      .pipe(
        map((isUpdate: any) => {
          return true;
        }
        ),
      );
  }

  /**
 * Get user data
 */
  getOverview() {
    return this.http.get<boolean>(this.authUrl + '/auth/user')
      .pipe(
        map((userdata: any) => {
          return userdata;
        }
        ),
      );
  }

  /**
    Hubspot AUth
    Login
   */
  userAcessToken(data) {
    return this.http.post<boolean>("https://api.hubapi.com/oauth/v1/token", data)
      .pipe(
        map((userdata: any) => {
          return userdata;
        }
        ),
        // catchError(this.handleError)
      );
  }


  /**
 * Send Auth Acess Tokken
 */

  getConnection() {
    return this.http.get(this.authUrl + '/hubspot-connection')
      .pipe(
        map((userdata: any) => {
          return userdata;
        }
        ),
        // catchError(this.handleError)
      );
  }


  /**
   * Send Auth Acess Tokken
   */

  sendAuthToken(data: hubauthAcess) {
    return this.http.post(this.authUrl + '/hubspot-authentication', data)
      .pipe(
        map((userdata: any) => {
          return true;
        }
        ),
        // catchError(this.handleError)
      );
  }

  /**
   * Event on user logout
   */
  logout() {
    this.loggedIn.next(false);
    localStorage.removeItem('token');
    localStorage.removeItem('currentUser');
    localStorage.clear();
    this.router.navigate(['/login']);
  }

}
