import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { User, SignupUser, ForgotUser, changePwd, overview, hubauthAcess } from '../../shared/models/user';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BackupService {
  private authUrl: string;
  constructor(
    private router: Router,
    private http: HttpClient
  ) {
    this.authUrl = environment.APIURL;

  }

  /**
   * Stripe Payment
   */

  hubspotPayment(data) {
    return this.http.post<boolean>(this.authUrl + '/stripe', data)
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }


  /**
   * Hubspot Backup Detail API **************************************************************************
   * DashBoard Graph And Backup Slider
   */
  hubspotbackup() {
    return this.http.get<boolean>(this.authUrl + '/hubspot-backup')
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }


  /**
 * Hubspot Backup History API ****************************************************************************
 */
  backuphistory(id) {
    return this.http.get<boolean>(this.authUrl + '/hubspot-backup-history/' + id)
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }


  /**
   * Backup Progess Bar Index Page API'S
   * On Index page wile backed up ************************************************************************
   */
  backupprogressbar() {
    return this.http.get<boolean>(this.authUrl + '/hubspot-backup-progress')
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }

  /**
   * Backup List 
   * On Index Page
   */
  backupDetails(data) {
    let val = {
      page: data
    }
    return this.http.post<boolean>(this.authUrl + '/dashboard/all-backups', val)
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }

  /**
   * Error list
   * Of Backups 
   */
  errbackupDetails(data) {
    let val = {
      page: data
    }
    return this.http.post<boolean>(this.authUrl + '/dashboard/error-backups', val)
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }


  /**
  * Success  list
  * Of Backups 
  */
  successBackups(data) {
    let val = {
      page: data
    }
    return this.http.post<boolean>(this.authUrl + '/dashboard/success-backups', val)
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }


  /**
* Warning  list
* Of Backups 
*/
  warningBackups(data) {
    let val = {
      page: data
    }
    return this.http.post<boolean>(this.authUrl + '/dashboard/warning-backups', val)
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }


  /**
  * Running  list
  * Of Backups 
  */
  runningBackups(data) {
    let val = {
      page: data
    }
    return this.http.post<boolean>(this.authUrl + '/dashboard/running-backups', val)
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }

  /**
   * Get Backup By Id ************************************************************************************
   */

  getBackupByid(id) {
    return this.http.get<boolean>(this.authUrl + '/dashboard/get-backup/' + id)
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }


  /**
   * Notifications Page API'S
   * Notification ****************************************************************************************
   * Listing
   */
  notification() {
    return this.http.get<boolean>(this.authUrl + '/notifications')
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }


  /**
   * Delete Notification destroy-notifications
   */

  deleteNotification(data) {
    return this.http.post<boolean>(this.authUrl + '/notifications/destroy', data)
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }

  /**
 * Delete Notification destroy-notifications
 */

  showNotification(id) {
    return this.http.get<boolean>(this.authUrl + '/notifications/show/' + id)
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }


  notificationscount() {
    return this.http.get<boolean>(this.authUrl + '/notifications/unread')
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }
  /**
   * Backup Now 
   * From Side bar bUTTON
   */
  backupNow() {
    return this.http.get<boolean>(this.authUrl + '/backup-now')
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }


  /**
   * Smart Page API'S
   *  Add smart Alert  **********************************************************************************
  */
  smartAlert(data) {
    return this.http.post<boolean>(this.authUrl + '/smartalert/create', data)
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }

  /**
   * Get Smart Alert
   */

  getsmartAlert() {
    return this.http.get<boolean>(this.authUrl + '/smartalert')
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }


  /**
 * Delete alert destroy-alert
 */
  deletealert(data) {
    return this.http.post<boolean>(this.authUrl + '/smartalert/destroy', data)
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }

  /**
   * Get Smart Alert History
   */

  gethistory() {
    return this.http.get<boolean>(this.authUrl + '/smartalert/history')
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }

  /**
 * Delete  Smart History notifications
 */
  deletealerthistory(data) {
    return this.http.post<boolean>(this.authUrl + '/smartalert/history/destroy', data)
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }

  getsmartbject() {
    return this.http.get<boolean>(this.authUrl + '/smartalert/objects')
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }

  /**  
   * Find Backups *********************************************************************
   */

  searchQuery(data) {
    return this.http.post<boolean>(this.authUrl + '/search', data)
      .pipe(
        map((data: any) => {
          return data;
        }),
      );
  }

}
