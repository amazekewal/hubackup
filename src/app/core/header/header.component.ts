import { Component, OnInit, Input, Output } from '@angular/core';
import { RouterModule, Routes, Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { BackupService } from '../services/backup.service';
import { ToastrService } from 'ngx-toastr';
import { StripeCheckoutLoader, StripeCheckoutHandler } from 'ng-stripe-checkout';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-main-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public price: number = 25000;
  public price1: number = 50000;
  // public price2: number = 150000;


  public payment: boolean = false;
  isLoggedIn$: Observable<boolean>;
  isNotify: Observable<boolean>;
  public currentUser: any;
  public showLanguage = 'en';
  userEmail: any;
  public notification: any = '';
  public notiList: any = '';
  loading: boolean = false;
  public stripePrice: any;
  private stripeCheckoutHandler: StripeCheckoutHandler;
  constructor(
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private router: Router,
    private authService: AuthService,
    private backupService: BackupService,
    public translate: TranslateService,
    
    private stripeCheckoutLoader: StripeCheckoutLoader,
  ) {


    let userData = localStorage.getItem('currentUser');
    if (userData) {
      let currentUser = JSON.parse(userData);
      if (currentUser.payment != null) {
        this.payment = true;
      }
    }

    this.isLoggedIn$ = this.authService.isLoggedIn;
  }


  /**
   * Change language switcher
   * @param language 
   */
  public switchLanguage(language) {
    this.translate.use(language);
    this.showLanguage = language;
    localStorage.setItem("language", this.showLanguage);
  }


  getcount() {
    this.backupService.notificationscount().subscribe((res: any) => {
      if (res.count) {
        this.notification = res.count;
        this.notiList = res.notifications
      }
    });
  }




  /**
   * Stripe Payment
   */
  public ngAfterViewInit() {
    this.stripeCheckoutLoader.createHandler({
      key: 'pk_test_H4c2B697tFYtUBAbsWbuoplU',
      name: 'Pricing',
      description: 'Hubbackup Subscriptions',
      token: (token) => {
      }
    }).then((handler: StripeCheckoutHandler) => {
      this.stripeCheckoutHandler = handler;
    });
  }


  /**
   * Hubackup Stripe Payment
   * @param data 
   */
  buynnoy(data: number) {
    this.stripePrice = data;
    this.stripeCheckoutHandler.open({

      amount: this.stripePrice,
      currency: 'USD',
    }).then((token) => {
      if (this.stripePrice == 25000) {
        this.stripePrice = 250;
      } else if (this.stripePrice == 50000) {
        this.stripePrice = 500;
      }
      let data = {
        token: token.id,
        amount: this.stripePrice
      }
      this.backupService.hubspotPayment(data).subscribe((res: any) => {
        if (res) {
          localStorage.setItem('payment', 'Done');
          let localStorageJsonData = JSON.parse(localStorage.getItem('currentUser'));
          localStorageJsonData.payment = 1;
          localStorage.setItem("currentUser", JSON.stringify(localStorageJsonData));
          this.paymentSuccess();
          this.payment = true;
          this.authService.onpayment();
        }
      });

    }).catch((err) => {
      if (err !== 'stripe_closed') {
        throw err;
      }
    });
  }


  /** Close Model */
  closeModal() {
    let element: HTMLElement = document.getElementById('clsmodal') as HTMLElement;
    element.click();
  }

  public onClickCancel() {
    this.stripeCheckoutHandler.close();
  }


  /**
   * Check UserLogin
   */
  checkUserLogin() {
    this.authService.tokenLogin.subscribe((data: any) => {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if (this.currentUser) {
        this.userEmail = this.currentUser.email;
      } else {
        this.userEmail = ' ';
      }
      if (this.currentUser.payment != null) {
        this.payment = true;
      }
    });
  }


  ngOnInit() {
    /**  Updrade Account */
    this.authService.upgradeHubackup.subscribe((data: any) => {
      let element: HTMLElement = document.getElementById('backUpnow') as HTMLElement;
      element.click();
    });

    this.checkUserLogin();


    /** Count Notifications */
    this.getcount();
    this.authService.notifyCount.subscribe((name: string) => {
      this.notification = 0;
      this.notiList = [];
      this.getcount();
    });
    // Get current user info
    if (this.isLoggedIn$) {
      this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if (this.currentUser) {
        this.userEmail = this.currentUser.email;
      } else {
        this.userEmail = ' ';
      }

    }
  }


  /**
 * Backup Now
 */
  backupNow() {
    let element: HTMLElement = document.getElementById('clsBtn') as HTMLElement;
    element.click();
    this.loading = true;
    this.backupService.backupNow().subscribe((res: any) => {
      if (res.status == 'Backup completed') {
        this.loading = false;
        this.showSuccess();
      }
      else if (res.status == 'Token is null') {
        this.loading = false;
        this.errormsg(res.status);
      } else if (res.status == 'Authentication error. Please Re-authenticate with Hubspot') {
        this.loading = false;
        this.errormsg(res.status);
      }
    });
  }


  /**
 * Sucessfully toaster Message
 * On Backup
 */
  showSuccess() {
    this.toastr.success('Backup Successfully', 'Complete');
  }

  /** On payment  Success */
  paymentSuccess() {
    this.toastr.success('Payment Successfully', 'Complete');
  }

  /**
   * Show Error Message 
   * On Tokken Error
   * @param data 
   */
  errormsg(data) {
    this.toastr.error(data, 'Warning');
  }

  /**
   * Logout
   */
  logout() {
    this.authService.logout();
  }

}
