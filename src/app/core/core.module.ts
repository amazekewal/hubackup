import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guard/auth-guard';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpIntercepterService } from './intercepter/http-intercepter';


import { TranslateModule } from '@ngx-translate/core';
import { DashboardModule } from '../modules/dashboard/dashboard.module';
import { StripeCheckoutModule } from 'ng-stripe-checkout';
import { MatButtonModule } from '@angular/material';
import { ToastrModule } from 'ngx-toastr';


@NgModule({
  declarations: [HeaderComponent, FooterComponent],
  imports: [
    CommonModule,
    RouterModule,
    TranslateModule.forChild({}),
    StripeCheckoutModule,
    MatButtonModule,
    ToastrModule.forRoot()
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    TranslateModule,
  ],
  providers: [
    AuthGuard,
    { provide: HTTP_INTERCEPTORS, useClass: HttpIntercepterService, multi: true },
  ]
})
export class CoreModule { }
