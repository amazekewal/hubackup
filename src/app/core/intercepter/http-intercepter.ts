import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpErrorResponse, HttpEvent } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../services/auth.service';


@Injectable({
  providedIn: 'root'
})

export class HttpIntercepterService implements HttpInterceptor {
  codeHubspot: any;
  contentType: string;
  constructor(
    private toastr: ToastrService,
    private router: Router,
    private authService: AuthService,
    private route: ActivatedRoute,
  ) {
    this.route.queryParams.subscribe(params => {
      this.codeHubspot = params.code;
    });
  }



  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let currentUserToken = JSON.parse(localStorage.getItem('token'));
    if (currentUser && currentUserToken) {
      let matchapi = req.url.match('hubspot-authentication');
      if (this.codeHubspot && matchapi == null) {
        this.contentType = 'application/x-www-form-urlencoded; charset=utf-8';
      } else {
        this.contentType = 'application/json';
      }
      req = req.clone({
        setHeaders: {
          Authorization: `Bearer ${currentUserToken}`,
          'Content-Type': this.contentType,
        }
      });
    }
    const changedReq = req;
    return next.handle(changedReq).pipe(catchError((err: any) => {
      const error = err.error.message || err.message;  // to send error message if error occurs in web api
      // console.log(err.error);
      if (err.error.error.code && err.error.error.code == '23000') {
        this.showError('Email Already exist');
      } else if (err.error.error.message && err.error.error.message != 'Token not provided') {
        this.showError(err.error.error.message);
      }
      if (err.error.error.message && err.error.error.message == 'Token not provided') {
        this.authService.logout();
        this.authService.ontokenExpire();
        this.router.navigate(['login']);
      }
      if (err.error.error.message && err.error.error.message == 'Token has expired') { // If jwt expired
        this.authService.logout();
        this.authService.ontokenExpire();
        this.router.navigate(['login']);
      }
      if (err.error.error.messag && err.error.error.message == "Trying to get property 'id' of non-object") {
        this.authService.logout();
        this.router.navigate(['login']);
      }
      return throwError('Http Error');
    }));
  }


  /**
  * Sucessfully toaster message
  */
  showError(data) {
    this.toastr.error(data, 'Failed');
  }
}
