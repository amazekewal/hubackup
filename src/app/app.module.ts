import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgxCarouselModule } from 'ngx-carousel';
import { HighchartsChartModule } from 'highcharts-angular';
import 'hammerjs';


import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { DashboardModule } from './modules/dashboard/dashboard.module';
import { BackupModule } from './modules/backup/backup.module';
import { CoreModule } from './core/core.module';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { ToastrModule } from 'ngx-toastr';
import { SettingModule } from './modules/setting/setting.module';
import { HttpModule } from '@angular/http';
import { AdminModule } from './admin/admin.module';
import { DataTablesModule } from 'angular-datatables';

// import { NgxStripeModule } from 'ngx-stripe';

declare var stripe: any;
declare var elements: any;
//factory to load the multilanguage json files  
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AuthModule,
    AppRoutingModule,
    // translater module with provider 
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader, 
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    CoreModule,
    BrowserAnimationsModule,
    DashboardModule,
    BackupModule,
    NgxCarouselModule,
    HighchartsChartModule,
    BsDatepickerModule.forRoot(),
    ToastrModule.forRoot(),
    SettingModule,
    HttpModule,
    AdminModule,
    DataTablesModule
    
    
  ],
  exports: [
    // TranslateModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
