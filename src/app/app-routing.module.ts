import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { SettingComponent } from './modules/setting/setting.component';
import { AuthGuard } from './core/guard/auth-guard';


const routes: Routes = [

  { path: 'admin', loadChildren: './admin/admin.module#AdminModule', canActivate: [AuthGuard], data: {
    title: 'Home'
  } },
  {
    path: 'auth',
    loadChildren: './auth/auth.module#AuthModule',
  },
  {
    path: 'dashboard',
    component: DashboardComponent, canActivate: [AuthGuard],
    children: [
      { path: '', loadChildren: './modules/dashboard/dashboard.module#DashboardModule', }
    ]
  },

  {
    path: 'setting',
    component: SettingComponent, canActivate: [AuthGuard]
  },

  {
    path: '',
    redirectTo: '/auth/login',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule] 
})
export class AppRoutingModule { }
