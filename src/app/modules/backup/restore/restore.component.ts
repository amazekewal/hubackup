import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-restore',
  templateUrl: './restore.component.html',
  styleUrls: ['./restore.component.scss']
})
export class RestoreComponent implements OnInit {

  listMenus = [{id : 1},{id : 1},{id : 1},{id : 1},{id : 1},{id : 1},{id : 1},{id : 1},{id : 1},{id : 1},{id : 1},
    {id : 1},{id : 1},{id : 1},{id : 1}
  ];

  //Acordian
  accordian = [{id : 1},{id : 1},{id : 1},{id : 1},{id : 1},{id : 1}];

  panelOpenState = false;
  icon: boolean = false;


  //Set index value of step
  public indexvalue = 0;

  /**
    Steps name array
   */
  stepsname = [
    {name : 'Select Source and repair type', value : 1},
    {name : 'Summary', value : 2}
  ];

  // Accordian
  click(){
    this.icon = !this.icon;
  } 


  constructor() { }

  ngOnInit() {
  }

}
