import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FindComponent } from './find/find.component';
import { RestoreComponent } from './restore/restore.component';
import { AuthGuard } from '../../core/guard/auth-guard';

// Header pages routes
const routes: Routes = [
  { path: 'find', component: FindComponent, canActivate: [AuthGuard] },
  { path: 'restore', component: RestoreComponent, canActivate: [AuthGuard] },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BackupRoutingModule { }
