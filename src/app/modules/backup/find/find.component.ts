import { Component, OnInit, ViewChild } from '@angular/core';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BackupService } from 'src/app/core/services/backup.service';
// import { DatePipe } from '@angular/common';



@Component({
  selector: 'app-find',
  templateUrl: './find.component.html',
  styleUrls: ['./find.component.scss']
})
export class FindComponent implements OnInit {
  findForm: FormGroup;
  loading: boolean = false;
  public backups: Array<any>;
  public searchData: any;
  public displayedColumns: any;
  public startDate: any;
  public endDate: any;
  public totalRecords: any;
  public recordDetails: any;
  public searchText: string;


  // Set index value
  public indexvalue = 0;

  /**
    Step Names array
   */
  stepsname = [
    { name: 'Select Backup Service', value: 1 },
    { name: 'Select Time Frame', value: 2 },
    { name: 'Enter Search Term', value: 3 },
    { name: 'Summary', value: 4 },
  ];



  constructor(
    private fb: FormBuilder,
    private backupService: BackupService,
    // private datePipe: DatePipe
  ) {
    this.createForm();
    this.getbackups();
  }

  // filterData(data){
  //   console.log(this.searchText);
  //   data = data.filter(function (item) {
  //     return !Object.values(item).map(function (value) {
  //       return String(value);
  //     }).find(function (value) {
  //       return value.includes(data);
  //     });
  //   });
  // }

  /** Create Find Form */
  createForm() {
    this.findForm = this.fb.group({
      search_table: ['', Validators.required],
      date: ['', Validators.required],
      search_term: ['', Validators.required],
    });
  }


  /**
   * Get backupsList
   * @param params 
   */




  getbackups() {
    this.backupService.getsmartbject().subscribe((res: any) => {
      if (res.objects.length > 0) {
        this.backups = res.objects;
      }
    });
  }

  /**
   * Submit Seach 
   * @param params 
   */
  onSubmit(params: { valid: boolean, value: any }) {
    if (params.valid) {
      this.indexvalue = this.indexvalue + 1;
      this.loading = true;
      this.backupService.searchQuery(params.value).subscribe((res) => {
        if (res.data) {
          this.displayedColumns = res.data.fields;
          this.searchData = res.data.data;
          console.log(this.searchData)
          this.startDate = res.data.startDate;
          this.endDate = res.data.endDate;
          this.totalRecords = res.data.recordsTotal;
          this.loading = false;
        } else {
          this.loading = false;
        }
      });
    }
  }

  recordDetail(data) {
    this.recordDetails = data;
    let element: HTMLElement = document.getElementById('viewBtn') as HTMLElement;
    element.click();
  }

  ngOnInit() {
  }

}
