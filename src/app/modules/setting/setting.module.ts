import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SecurityComponent } from './security/security.component';
import { OverviewComponent } from './overview/overview.component';
import { SettingComponent } from './setting.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import {StripeCheckoutModule} from 'ng-stripe-checkout';
import { MatSelectModule, MatInputModule, MatIconModule, MatButtonModule, MatTableModule, } from '@angular/material';


@NgModule({
  declarations: [SecurityComponent, OverviewComponent, SettingComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatSelectModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    StripeCheckoutModule

  ],
  bootstrap: [SettingComponent]
})
export class SettingModule { }
