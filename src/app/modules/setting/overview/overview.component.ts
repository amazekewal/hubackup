import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { overview } from '../../../shared/models/user';
import { AuthService } from '../../../core/services/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.scss']
})
export class OverviewComponent implements OnInit {
  overviewForm: FormGroup;
  public emailid: any;


  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private toastr: ToastrService
  ) {
    // this.overviewForm.disable();
    this.createform();
    this.getuserData1();
  }


  /**
   * Get user data on overview
   */
  public getuserData1() {
    this.authService.getOverview()
      .subscribe(res => {

        // console.log(res);
        this.emailid = res.email;
        this.overviewForm.patchValue({
          companyName: res.companyName,
          code: res.code,
          phoneNumber: res.phoneNumber,
          email: res.email,
          timeZone: res.timeZone,
          country: res.country,
          state: res.state,
          address: res.address,
          address1: res.address1,
          address2: res.address2,
          billingAddress: res.billingAddress,
          billingAddress1: res.billingAddress1,
          billingAddress2: res.billingAddress2,
        });
      });
  }


  /**
 * Overview form submit
 */
  onSubmit(params: { valid: boolean, value: overview }) {
    if (params.valid) {
      this.authService.updateOverview(params.value)
        .subscribe(res => {
          this.showSuccess();
        });
    }
  }


  /**
  Create overview Form
   */
  createform() {
    this.overviewForm = this.fb.group({
      companyName: ['', Validators.required],
      code: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      timeZone: ['', Validators.required],
      country: ['', Validators.required],
      state: ['', Validators.required],
      address: ['', Validators.required],
      address1: ['', Validators.required],
      address2: ['', Validators.required],
      billingAddress: ['', Validators.required],
      billingAddress1: ['', Validators.required],
      billingAddress2: ['', Validators.required],
    });
  }


  /**
 * Sucessfully toaster message
 */
  showSuccess() {
    this.toastr.success('Update Sucessfully', 'Thank you');
  }

  ngOnInit() {

  }

}
