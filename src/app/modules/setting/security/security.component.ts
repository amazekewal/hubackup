import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-security',
  templateUrl: './security.component.html',
  styleUrls: ['./security.component.scss']
})
export class SecurityComponent implements OnInit {

  hide = true;
  security : FormGroup;
  constructor(private fb: FormBuilder) { }

    // Create formgroup
    createForm() {
      this.security = this.fb.group({
        email: ['', [Validators.email]],
        password: ['', Validators.required],
      })
    }

  ngOnInit() {
    this.createForm();
  }

}
