import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentBackupComponent } from './recent-backup.component';

describe('RecentBackupComponent', () => {
  let component: RecentBackupComponent;
  let fixture: ComponentFixture<RecentBackupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentBackupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentBackupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
