import { Component, OnInit, ɵConsole } from '@angular/core';
import { NgxCarousel } from 'ngx-carousel';
import { BackupService } from 'src/app/core/services/backup.service';

@Component({
  selector: 'app-recent-backup',
  templateUrl: './recent-backup.component.html',
  styleUrls: ['./recent-backup.component.scss'],
  
})
export class RecentBackupComponent implements OnInit {
  public itemsList : any = '';
  public carouselTileItems: Array<any>;
  public carouselTile: NgxCarousel;
  constructor(
    private Backupservice: BackupService
  ) {
    this.Backupservice.hubspotbackup().subscribe((res: any) => {
      this.itemsList = res.backup_chunk_dates;
  });
  }

  ngOnInit() {

    // Carousel slider
    this.carouselTileItems = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];
    this.carouselTile = {
      grid: {xs: 2, sm: 3, md: 3, lg: 4, all: 0},
      slide: 2,
      speed: 400, 
      animation: 'lazy',
      point: {
        visible: false,
      },
      load: 2,
      touch: true,
      easing: 'ease'
    }
  }

}
  