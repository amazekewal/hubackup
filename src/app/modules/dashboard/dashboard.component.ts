import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpHeaders, HttpClient, HttpClientModule } from '@angular/common/http';
import { hubSpotAcess } from '../../shared/models/user';
import { ToastrService } from 'ngx-toastr';
import { AuthService } from '../../core/services/auth.service';
import { BackupService } from 'src/app/core/services/backup.service';





@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  codeHubspot: any;
  loading: boolean = false;
  payment: boolean = false;
  public lastdate ;
  public lasttime ;
  public backupId : any ;

  constructor(

    private route: ActivatedRoute,
    public http: HttpClient,
    private authService: AuthService,
    private Backupservice: BackupService,
    private toastr: ToastrService
  ) {

    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(currentUser.payment != null){
      this.payment =  true;
    }
    this.route.queryParams.subscribe(params => {
      if (params.id) {
        this.backupId =  params.id;
        this.Backupservice.getBackupByid(params.id).subscribe((res: any) => {
          if(res.backup_date){
            // console.log(res);
            this.lastdate = res.backup_date;
            this.lasttime = res.backup_time;
          }
        });
      }

    });
    /** 
      Hubspot api
      Auth
     */
    this.route.queryParams.subscribe(params => {
      // Get login token

      this.codeHubspot = params.code;
      let HubAuthcode = localStorage.getItem('HubAuthcode');

      if (this.codeHubspot && this.codeHubspot !== HubAuthcode) {
        // Set Auth code to local storage
        localStorage.setItem('HubAuthcode', this.codeHubspot);
        let body = new URLSearchParams();
        body.set('grant_type', 'authorization_code');
        body.set('client_id', '889a2161-1aa0-4842-8e05-b7e87cf4a39f');
        body.set('client_secret', '8ce8de3e-f210-45d5-83c0-94555b0b9f30');
        body.set('redirect_uri', 'https://app.hubackup.com/dashboard/backup');
        body.set('code', this.codeHubspot);

        // Get acess token and refress token
        this.authService.userAcessToken(body.toString())
          .subscribe((data: any) => {
            if (data.access_token) {
              let param: any = {
                token: data.access_token,
                refresh: data.refresh_token
              }
              this.authService.sendAuthToken(param).subscribe((res: any) => {
                if (res) {
                  this.showAuthSuccess();
                }
              })
            }
          });
      }
    });
  }


  /** If payment 
   * Not Done
   */
  upgradeNow(){
    this.authService.upgradeAccount();
  }


  /**
   * Backup Now
   */
  backupNow() {
    // let currentUser =  JSON.parse(localStorage.getItem('currentUser'));
    this.loading = true;
    this.Backupservice.backupNow().subscribe((res: any) => {
      if (res.status == 'Backup completed') {
        this.loading = false;
        this.showSuccess();
      }
      else if (res.status == 'Token is null') {
        this.loading = false;
        this.errormsg(res.status);
      } else if (res.status == 'Authentication error. Please Re-authenticate with Hubspot') {
        this.loading = false;
        this.errormsg(res.status);
      }
    });
  }


  /**
   * Sucessfully toaster Message
   * On Backup
   */
  showSuccess() {
    this.toastr.success('Backup Successfully', 'Complete');
  }

  /**
   * Show Error Message 
   * On Tokken Error
   * @param data 
   */
  errormsg(data) {
    this.toastr.error(data, 'Warning');
  }

  /**
   * Show Succesfull Message 
   * On Re-authenticate
   */
  showAuthSuccess() {
    this.toastr.success('Re-authenticate Successfully', 'Thank You');
  }

  ngOnInit() {
    this.authService.paymentSuccess.subscribe((name: string) => {
      this.payment =  true;
    });
  }

}