import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './index/index.component';
import { BackupComponent } from './backup/backup.component';
import { DashboardComponent } from './dashboard.component';
import { BackupHisoryComponent } from './backup-hisory/backup-hisory.component';
import { SmartAlertComponent } from './smart-alert/smart-alert.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { AuthGuard } from '../../core/guard/auth-guard';

const routes: Routes = [
  { path: 'index', component: IndexComponent },
  { path: 'backup', component: BackupComponent },
  { path: 'backup-history', component: BackupHisoryComponent },
  { path: 'smart-alert', component: SmartAlertComponent },
  { path: 'notification', component: NotificationsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
