import { Component, OnInit } from '@angular/core';
import { BackupService } from '../../../core/services/backup.service';
import * as Highcharts from 'highcharts';
import { HighchartsChartModule } from 'highcharts-angular';

@Component({
  selector: 'app-backup',
  templateUrl: './backup.component.html',
  styleUrls: ['./backup.component.scss']
})
export class BackupComponent implements OnInit {
  public lastRecords: any = '';
  loading : boolean = true;
  Highcharts: any;
  chartOptions: any;

  constructor(
    private Backupservice: BackupService,
    
  ) {
    this.Backupservice.hubspotbackup().subscribe((res: any) => {
      this.Highcharts = Highcharts;
      this.chartOptions = {
        chart: {
          type: 'areaspline'
        },
        title: {
          text: 'Statistics'
        },
        xAxis: {
          categories: res.backup_dates,
          tickmarkPlacement: 'on',
          title: {
            enabled: false
          }
        },
        yAxis: {
          // categories: ['N/A', 'Never', 'Seldom', 'Mostly', 'Always'],
          tickmarkPlacement: 'on',
          // title: {
          //   text: 'Percent'
          // }
        },
        legend: {
          layout: 'horizontal',
          align: 'right',
          verticalAlign: 'top',
          y: -40,
        },
        tooltip: {
          pointFormat: '{series.name} had stockpiled <b>{point.y:,.0f}</b><br/>warheads in {point.x}'
        },
        plotOptions: {
          area: {
            pointStart: 1940,
            marker: {
              enabled: false,
              symbol: 'circle',
              radius: 2,
              states: {
                hover: {
                  enabled: true
                }
              }
            }
          }
        },
        series: [{
          color: {
            linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
            stops: [
              [0, 'rgba(67, 209, 222, 0.8)'],
              [1, 'rgba(184, 237, 242, 0.8)']
            ]
          },
          name: 'Added',
          data: res.added_records
        },
        {
          color: {
            linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
            stops: [
              [0, 'rgba(204, 204, 255, 0.6)'],
              [1, 'rgba(255, 102, 204, 0.6)']
            ]
          },
          name: 'Changed',
          data: res.changed_records
        },
        {
          name: 'Removed',
          data: [
            null, null, null, null, null, null, null, null, null, null, null, null
          ]
        }],

      };
        this.lastRecords = res;
        this.loading = false;

    });


  }
  ngOnInit() {

  }

}
