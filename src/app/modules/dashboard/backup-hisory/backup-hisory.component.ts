import { Component, OnInit, ViewChild } from '@angular/core';
import { BackupService } from 'src/app/core/services/backup.service';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { ActivatedRoute } from '@angular/router';

// History table variables
export interface PeriodicElement {
  sobject: any;
  records: any;
  size: any;
  removed: any;
  changed: any;
  added: any;
}


@Component({
  selector: 'app-backup-hisory',
  templateUrl: './backup-hisory.component.html',
  styleUrls: ['./backup-hisory.component.scss']
})
export class BackupHisoryComponent implements OnInit {
  /**
   * Table Columns
   */
  displayedColumns: string[] = ['object_name', 'num_of_records', 'object_size', 'removed_records', 'added_records'];
  dataSource = new MatTableDataSource<PeriodicElement>();
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  history: any;
  statustitle: string;
  loading: boolean = true;
  constructor(
    private Backupservice: BackupService,
    private route: ActivatedRoute,
  ) {
    /**
     * Backup History Api
     * 
     */
    this.route.queryParams.subscribe(params => {
    if (params.id) {
        this.Backupservice.backuphistory(params.id).subscribe((res: any) => {
          if (res && res.status != 'no data') {
            this.dataSource.data = res.backup_history;
            this.history = res;
            this.loading = false;
          } else {
            this.statustitle = res.status;
            this.loading = false;
          }
        });
      }
    });
  }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }

}
