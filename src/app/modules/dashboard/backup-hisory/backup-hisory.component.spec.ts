import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackupHisoryComponent } from './backup-hisory.component';

describe('BackupHisoryComponent', () => {
  let component: BackupHisoryComponent;
  let fixture: ComponentFixture<BackupHisoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackupHisoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackupHisoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
