import { Component, OnInit, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { BackupService } from 'src/app/core/services/backup.service';
import { MatProgressBarModule } from '@angular/material';
import { Observable } from 'rxjs/Rx';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})

// Dashboard index page class
export class IndexComponent implements OnInit {
  public showAuth: boolean;
  public noData: string = '';
  public noData1: string = '';
  public progressData;
  public backupdata: any;
  public progress: boolean = false;
  public loading: boolean = true;
  public loadMoreBtn: boolean = false;
  public allBackup: any = '';
  public running: any = '';
  public postItems: any = 0;
  public indexVal: any = 1;
  public listType: string = '';

  // Load images length
  public loadmore: any = 6;
  currentUser: any;

  constructor(
    private router: Router,
    private Backupservice: BackupService,
    private authService: AuthService
  ) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    let payments = localStorage.getItem('payment');
    if (currentUser.payment == null || currentUser.payment == 1 || payments == '') {
      this.showAuth = false;
    }
     if(payments == 'Done') {
      this.showAuth = true;
    }

    // Progress Bar Api
    Observable.interval(0.1 * 60 * 1000).subscribe(x => {
      this.Backupservice.backupprogressbar().subscribe((res: any) => {
        if (res.status != 'done') {
          this.noData = null;
          this.progressData = res;
          this.progress = true;
        }else{
          this.noData1 = "No Running Backup List";
          this.loading = false;
          this.running = 0;
        }
      });
    });

    // Backup Details List
    this.Backupservice.backupDetails(this.postItems).subscribe((res: any) => {
      this.noData = null;
      if (res.dates) {
        let data = res.dates;
        this.allBackup = res.all_count;
        this.running = res.running_count;
        this.backupdata = data;
        this.loading = false;
        if (this.allBackup > 6) {
          this.loadMoreBtn = true;
        }

      } else {
        this.noData = "No Backup List";
        this.loading = false;
      }
    });


  }


  /**
   * Running Backup 
   * Function
   */
  runningBackup(){
    this.Backupservice.backupprogressbar().subscribe((res: any) => {
      if (res.status != 'done') {
        this.progressData = res;
        this.loading = false;
        this.progress = true;
      }else{
        this.noData1 = "No Running Backup List";
        this.loading = false;
        this.running = 0;
      }
    });
  }

  /**
   * Show Hubspot Auth Model 
   * If user Login First Time
   */
  authLogin() {
    this.showAuth = false;
    localStorage.removeItem('payment');
    window.open('https://app.hubspot.com/oauth/authorize?scope=contacts%20content%20oauth%20forms%20files&redirect_uri=https://app.hubackup.com/dashboard/backup&client_id=889a2161-1aa0-4842-8e05-b7e87cf4a39f', '_blank');
  }

  /**
   * Load More Details
   * @param data 
   * @param type 
   */
  loadMore(data, type) {
    if (type == 'all' || type == '') {
      this.indexVal++;
      this.Backupservice.backupDetails(data).subscribe((res: any) => {
        if (res.dates) {
          res.dates.forEach(i => {
            this.backupdata.push(
              {
                id : i.id,
                date: i.date,
                time: i.time
              });
          });
        }
        if (res.last_data == true) {
          this.loadMoreBtn = false;
        }
      });
    }
  }



  /**
   * All Backups
   * Listing
   */

  allBackupsList() {
    this.noData = null;
    this.noData1 = null;
    this.backupdata = null;
    this.loadMoreBtn = false;
    this.indexVal = '1';
    this.listType = 'all';
    this.loading = true;
    this.Backupservice.backupDetails(this.postItems).subscribe((res: any) => {
      if (res.dates.length > 0) {
        let data = res.dates;

        this.backupdata = data;
        this.loading = false;
        if (this.allBackup > 6) {
          this.loadMoreBtn = true;
        }
      } else {
        this.noData = "No Backup List";
        this.loading = false;
      }
    });
  }

  /**
   * All Running Backup List
   * Listing
   */

  runningBackupsList() {
    this.noData1 = null;
    this.progress = false;
    this.progressData = null
    this.listType = 'running';
    this.loading = true;
    this.runningBackup();
  }


  backupDetails(id) {
    if (id) {
      this.router.navigate(['/dashboard/backup'], { queryParams: { id: id } });
    }
  }


  ngOnInit() {
    this.authService.paymentSuccess.subscribe((name: string) => {
      this.showAuth = true;
    });
  }

}
