import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BackupService } from 'src/app/core/services/backup.service';
import { ToastrService } from 'ngx-toastr';

// Smart alert table variable
export interface PeriodicElement {
  sobject: any;
  action: any;
  operator: any;
  amount: any;
  unit: any;
  delete: any;
}


@Component({
  selector: 'app-smart-alert',
  templateUrl: './smart-alert.component.html',
  styleUrls: ['./smart-alert.component.scss']
})
export class SmartAlertComponent implements OnInit {
  smartForm: FormGroup;
  public loading: boolean = true;
  public payment: boolean = false;
  public dataSource: any = '';
  public historySource: any = '';
  public smartObject: any = '';

  // Table headings
  displayedColumns: string[] = ['sobject', 'action', 'operator', 'amount', 'unit', 'delete'];


  constructor(
    private fb: FormBuilder,
    private Backupservice: BackupService,
    private toastr: ToastrService
  ) {

    let userData = localStorage.getItem('currentUser');
    if (userData) {
      let currentUser = JSON.parse(userData);
      if (currentUser.payment != null) {
        this.payment = true;
      }
    }

    this.createForm();
    this.getsmartAlert();
    this.getHistory();
    this.getsmartObject();

  }

  /**
   * Get smart alert
   * List
   */
  getsmartAlert() {
    this.Backupservice.getsmartAlert().subscribe((res: any) => {
      if (res) {
        this.dataSource = res.smartalerts;
        this.loading = false;
      }
    });
  }

  getsmartObject() {
    this.Backupservice.getsmartbject().subscribe((res: any) => {
      if (res.objects.length > 0) {
        // console.log(res);
        this.smartObject = res.objects;
      }
    });
  }


  /**
   * Delete Smart Alert
   */

  deleteSmartalert(data) {
    if (data) {
      let arr: any = {
        id: data
      }
      this.Backupservice.deletealert(arr).subscribe((res: any) => {
        if (res) {
          this.getsmartAlert();
          this.showSuccessOndelete();
        }
      });
    }
  }


  /**
   * Get History List
   */
  getHistory() {
    this.Backupservice.gethistory().subscribe((res: any) => {
      if (res.smartalerts) {
        this.historySource = res.smartalerts;
      }
    });
  }

  /**
   * Delete History List
   */
  deletehistory(data) {
    if (data) {
      let arr: any = {
        id: data
      }
      this.Backupservice.deletealerthistory(arr).subscribe((res: any) => {
        if (res) {
          this.getHistory();
          this.showOndeleteHistory();
        }
      });
    }
  }

  /**
   * Create smart alert
   * Form
   */
  createForm() {
    this.smartForm = this.fb.group({
      operator: ['', Validators.required],
      amount: ['', Validators.required],
      s_object: ['', Validators.required],
      action: ['', Validators.required],
    });
  }


  /**
   * Add Smart Alert
   * @param params 
   */
  onSubmit(params: { valid: boolean, value: any }) {
    if (params.valid) {
      this.Backupservice.smartAlert(params.value).subscribe((res: any) => {
        if (res.status == 'error') {
          this.showalertWarning();
        } else {
          this.getsmartAlert();
          this.showSuccess();
        }
      });
    }
  }


  /**
 * Sucessfully toaster message
 */
  showSuccess() {
    this.toastr.success('Smart Alert Added Sucessfully', 'Added');
  }

  showSuccessOndelete() {
    this.toastr.success('Smart Alert Deleted Sucessfully', 'Thank You');
  }
  showOndeleteHistory() {
    this.toastr.success('History Deleted Sucessfully', 'Thank You');
  }

  showalertWarning() {
    this.toastr.info('Alert is already added', 'Warning');
  }

  ngOnInit() {
  }

}
