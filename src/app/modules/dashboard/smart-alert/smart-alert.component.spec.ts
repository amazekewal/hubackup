import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartAlertComponent } from './smart-alert.component';

describe('SmartAlertComponent', () => {
  let component: SmartAlertComponent;
  let fixture: ComponentFixture<SmartAlertComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartAlertComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartAlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
