import { Component, OnInit, ViewChild, EventEmitter, Output, Input } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { BackupService } from 'src/app/core/services/backup.service';
import { ToastrService } from 'ngx-toastr';
import { BehaviorSubject } from 'rxjs';
import { AuthService } from 'src/app/core/services/auth.service';


// Notifications table variables
export interface PeriodicElement {
  id: any;
  title: any;
  type: any;
  action: any;
}

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.scss']
})
export class NotificationsComponent implements OnInit {
  @Output() myEvent = new EventEmitter();
  public payment : boolean = false;
  public loading: boolean = true;
  public viewData;
  public notViewed = '';
  public countIncrease: any = 1;

  dataSource = new MatTableDataSource<PeriodicElement>();
  displayedColumns: string[] = ['select', 'id', 'title', 'type', 'action'];
  selection = new SelectionModel<PeriodicElement>(true, []);
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  public selected: Array<any> = [];
  public hideCheckbox: boolean = true;

  constructor(
    private Backupservice: BackupService,
    private toastr: ToastrService,
    private authService: AuthService
  ) {
    let userData = localStorage.getItem('currentUser');
    if(userData){
      let currentUser = JSON.parse(userData);
      if(currentUser.payment != null){
        this.payment =  true;
      }
    }
    this.getnotifications();
  }

  notify() {
    this.authService.notifyData();
  }
  getnotifications() {
    this.Backupservice.notification().subscribe((res: any) => {
      if (res) {
        // console.log(res);
        this.dataSource.data = res.notifications;
        this.dataSource.paginator = this.paginator;
        this.selection = new SelectionModel<PeriodicElement>(true, []);
        this.loading = false;
      }
    });
  }


  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSource.data.forEach(row => this.selection.select(row));
  }

  /**
   * Delete Selected Notification
   */
  deleteAll() {
    let data = this.selection.selected;
    if (data.length > 0) {
      data.forEach(item => {
        this.selected.push(item.id);
      });
      let arr: any = {
        checked: this.selected
      }
      this.Backupservice.deleteNotification(arr).subscribe((res: any) => {
        if (res) {
          this.getnotifications();
          this.showSuccess();
        }
      });
    } else {
      this.warning();
    }
  }

  /**
  * Delete Single Notification
  */
  deleteRow(data) {
    this.hideCheckbox = false;
    this.selection.clear();
    if (data) {
      let arr: any = {
        checked: data
      }
      this.Backupservice.deleteNotification(arr).subscribe((res: any) => {
        if (res) {
          this.getnotifications();
          this.hideCheckbox = true;
        }
      });
    }
  }


  viewRow(event, id) {
    event.target.classList.remove('active');
    if (id) {
      this.Backupservice.showNotification(id).subscribe((res: any) => {
        if (res.notification) {
          // console.log(res.notification);
          this.viewData = res.notification;
          let element: HTMLElement = document.getElementById('viewBtn') as HTMLElement;
          element.click();
          this.authService.notifyData();
        }
      });
    }
  }

  /**
   * Sucessfully toaster message
   */
  showSuccess() {
    this.toastr.success('Notification Deleted Successfully', 'Complete');
  }
  warning() {
    this.toastr.warning('No Notification Selected', 'warning');
  }
  ngOnInit() {
  }


}
