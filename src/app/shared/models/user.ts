/**
 * Login user 
 */
export interface User {
  email: string;
  password: string;
  region: string;
}

/**
 * Signup user
 */
export interface SignupUser {
  package: any;
  firstName: string;
  email: string;
  password: string;
  lastName : string;
  companyName: string;
  address: string;
  city: string;
  state: string;
  zipcode: string;
}


/**
 * Forget Email
 */
export interface ForgotUser {
  email: string;
}

/**
 * Change password Email
 */
export interface changePwd {
  password : string;
  password_confirmation: string;
  token : string;
}


/**
 * overview profile
 */
export interface overview {
  companyName : string;
  code: string;
  phoneNumber : string;
  timeZone : string;
  country : string;
  state : string;
  address : string;
  address1 : string;
  address2 : string;
  billingAddress : string;
  billingAddress1 : string;
  billingAddress2 : string;
}


/**
 * Hubspot Values
 */
export interface hubSpotAcess {
  grant_type : string;
  client_id: string;
  client_secret : string;
  redirect_uri : string;
  code : string;
}

/**
 * Hubspot Values
 */
export interface hubauthAcess {
  access_token : string;
  expires_in : string;
  refresh_token : string;
}
