import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { AuthService } from './core/services/auth.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

/**
 * Class to manage the APP
 */
export class AppComponent {
  public subscription;
  public showLanguage = 'en';
  codeHubspot: any;
  item: number = 0;

  public currentUrl: boolean = true;

  /**
   * Initial setup while app is created
   * 
   * @param translate 
   * 
   */
  private publishableKey: string = "rk_test_LXNvLcka0v6kuwDM3S3rl3hP00vKjqM56Y";
  constructor(private translate: TranslateService,
    private authService: AuthService,
    private router: Router, private route: ActivatedRoute, private http: HttpClient) {
    this.router.events.subscribe((res) => {
      if (this.router.url == '/admin/user') {
        this.currentUrl = false;
        // console.log(this.router.url);
      } else {
        // console.log(this.router.url);
        this.currentUrl = true;
      }

    });
    this.showLanguage = localStorage.getItem('language') === null ? 'en' : localStorage.getItem('language');
    translate.setDefaultLang(this.showLanguage);
  }




  createProject(event: string) {
    console.log("create Project on parent component");
  }

  ngOnInit() {
    this.authService.tokenExpire.subscribe((data: any) => {
      this.router.events.subscribe((res) => {
        if (this.router.url == '/admin/user') {
          this.currentUrl = false;
        } else {
          this.currentUrl = true;
        }

      })
    });
  }
}
