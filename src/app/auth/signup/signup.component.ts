import { Component, OnInit, NgModule, ɵConsole } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from '../../shared/custom-validators';
import { User, SignupUser } from '../../shared/models/user';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../core/services/auth.service';
import { StripeCheckoutLoader, StripeCheckoutHandler } from 'ng-stripe-checkout';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {


  signupForm: FormGroup;



  /**
   * Pasword Field Text or password check
   */
  hide = true;
  currentUser: any;

  private stripeCheckoutHandler: StripeCheckoutHandler;
  constructor(
    private fb: FormBuilder,
    private toastr: ToastrService,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private stripeCheckoutLoader: StripeCheckoutLoader

  ) {

    this.createForm();
  }

  /************************************************************** Stripe Payment *********************** */
  // public ngAfterViewInit() {
  //   this.stripeCheckoutLoader.createHandler({
  //     key: 'pk_test_H4c2B697tFYtUBAbsWbuoplU',
  //     token: (token) => {
  //       console.log('Payment successful!', token);
  //     }
  //   }).then((handler: StripeCheckoutHandler) => {
  //     this.stripeCheckoutHandler = handler;
  //   });
  // }


  // buynnoy(){
  //   let ammount = 250;
  //     this.stripeCheckoutHandler.open({
  //       amount: ammount,
  //       currency: 'USD',
  //     }).then((token) => {


  //     }).catch((err) => {
  //       if (err !== 'stripe_closed') {
  //         throw err;
  //       }
  //     });
  // }


  // public onClickBuy() {
  //   this.stripeCheckoutHandler.open({
  //     amount: 1500,
  //     currency: 'USD',
  //   }).then((token) => {
  //     console.log('Payment successful!', token);
  //   }).catch((err) => {
  //     // Payment failed or was canceled by user...
  //     if (err !== 'stripe_closed') {
  //       throw err;
  //     }
  //   });
  // }

  // public onClickCancel() {
  //   this.stripeCheckoutHandler.close();
  // }

  /*************************************************************Stripe payment End ************************* */

  /**
   * Signup form submit
   */
  onSubmit(params: { valid: boolean, value: SignupUser }) {
    // console.log()
    if (params.valid) {
      this.authService.signup(params.value)
        .subscribe(res => {
          this.showSuccess();
          setTimeout(() => {
            // console.log('hide');
            this.router.navigate(['login']);
          }, 2000);
        });
    } else {

    }
  }

  /**
   * Create signup form
   */
  createForm() {
    this.signupForm = this.fb.group({
      email: ['', [Validators.required, CustomValidators.email]],
      firstName: ['', Validators.required],
      password: ['', Validators.required],
      lastName: ['', Validators.required],
      companyName: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      state: ['', Validators.required],
      zipcode: ['', Validators.required]
    })
  }



  ngOnInit() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (this.currentUser) {
      this.router.navigate(['index']);
    }
  }




  /**
 * Sucessfully toaster message
 */
  showSuccess() {
    this.toastr.success('Signup Sucessfully', 'Welcome');
  }

  // Count capital letters in string
  checkCapitalletter(data) {
    return data.replace(/[^A-Z]/g, "").length;
  }

  // Count numbers in string
  checkNumbers(data) {
    return data.replace(/[^0-9]/g, "").length;
  }

}
