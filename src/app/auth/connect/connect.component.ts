import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { IfStmt } from '@angular/compiler';
import { async } from 'q';

@Component({
  selector: 'app-connect',
  templateUrl: './connect.component.html',
  styleUrls: ['./connect.component.scss']
})
export class ConnectComponent implements OnInit {

  connetForm: FormGroup;
  public codeHubspot: string;
  public loading: boolean = false;
  constructor(
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private authService: AuthService,
    private fb: FormBuilder
  ) {

    this.authService.getConnection().subscribe((res: any) => {
      if (res.data) {
        this.connetForm.patchValue({
          token: res.data.accesstoken,
          connectionName: res.data.connectionName,
          refresh: res.data.refreshtoken
        });
      }

    });

    this.createForm();
  }

  /** Create Save Connect Form */
  createForm() {
    this.connetForm = this.fb.group({
      connectionName: ['', Validators.required],
      token: ['', Validators.required],
      refresh: ['', Validators.required],
    });
  }


  /**Save Auth Detail */
  saveForm(params: { valid: boolean, value: any }) {
    if (params.valid) {
      this.authService.sendAuthToken(params.value).subscribe((res: any) => {
        if (res) {
          this.showAuthsaveSuccess();
          let element: HTMLElement = document.getElementById('closeBtn') as HTMLElement;
          element.click();
        } else {
          this.showAuthsaveError();
        }
      })
    }
  }


  /** Auth Connect to Hubspot */
  authConnect() {
    let name: string = this.connetForm.value.connectionName;
    if (name) {
      localStorage.setItem('authname', name);
    }
    window.open('https://app.hubspot.com/oauth/authorize?scope=contacts%20content%20oauth%20forms%20files&redirect_uri=https://app.hubackup.com/auth/connect&client_id=889a2161-1aa0-4842-8e05-b7e87cf4a39f', '_blank');
  }


  /** 
  Hubspot api
  Auth
  */
  connectionHubspot() {
    this.route.queryParams.subscribe(params => {
      // Get login token
      this.codeHubspot = params.code;
      let HubAuthcode = localStorage.getItem('HubAuthcode');

      if (this.codeHubspot && this.codeHubspot !== HubAuthcode) {
        this.loading = true;
        // Set Auth code to local storage
        localStorage.setItem('HubAuthcode', this.codeHubspot);
        let body = new URLSearchParams();
        body.set('grant_type', 'authorization_code');
        body.set('client_id', '889a2161-1aa0-4842-8e05-b7e87cf4a39f');
        body.set('client_secret', '8ce8de3e-f210-45d5-83c0-94555b0b9f30');
        body.set('redirect_uri', 'https://app.hubackup.com/auth/connect');
        body.set('code', this.codeHubspot);
        // Get acess token and refress token
        this.authService.userAcessToken(body.toString())
          .subscribe((data: any) => {
            if (data.access_token) {
              this.loading = false;
              let name = localStorage.getItem('authname');
              setTimeout(() => {    //<<<---    using ()=> syntax
                let element: HTMLElement = document.getElementById('modelSHow') as HTMLElement;
                element.click();
                this.connetForm.patchValue({
                  token: data.access_token,
                  connectionName: name,
                  refresh: data.refresh_token
                });
              }, 500);
              localStorage.removeItem('authname');
              this.showAuthSuccess();
            }
          });
      }
    });
  }


  /**
  * Show Succesfull Message 
  * On Re-authenticate
  */
  showAuthSuccess() {
    this.toastr.success('Re-authenticate Successfully', 'Thank You');
  }
  showAuthsaveSuccess() {
    this.toastr.success('Save Auth Details Sucessfully', 'Thank You');
  }
  showAuthsaveError() {
    this.toastr.error('Save Auth Details Failed', 'Thank You');
  }

  ngOnInit() {
    this.connectionHubspot();
  }

}
