import { Component, OnInit, NgModule } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../core/services/auth.service';
import { CustomValidators } from '../../shared/custom-validators';
import { User } from '../../shared/models/user';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})


export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  public currentUser;


  // Pasword Field Text or password check
  hide = true;


  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private toastr: ToastrService
  ) { 

  }


  /**
   * On Login form submit
   * 
   * @param valid 
   * @param value 
   */
  onSubmit(params: { valid: boolean, value: User }) {
    if (params.valid) {
      this.authService.login(params.value)
        .subscribe((res : any) => {
          this.authService.onUserlogin();
          if (res === true) {
            this.showSuccess();
            this.router.navigate(['index'])
          }
        });
    } else {
      this.toastr.error('Invalid Form Values', 'Failed');  
    }
  }


  /**
   * Create Login form
   */
  createForm() {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required, CustomValidators.email]],
      password: ['', Validators.required]
      // region: ['', Validators.required]
    })
  }


  /**
   * Sucessfully toaster message
   */
  showSuccess() {
    this.toastr.success('Login Sucessfully', 'Welcome');
  }
  

  ngOnInit() {
    this.createForm();
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if(this.currentUser){
      this.router.navigate(['index']);
    }
    
  }

}
