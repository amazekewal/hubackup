import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatSelectModule, MatInputModule,  MatIconModule, MatButtonModule, MatSnackBarModule, MatCardModule} from '@angular/material';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { AuthRoutingModule } from './auth-routing.module';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { CoreModule } from '../core/core.module';
import { ToastrModule } from 'ngx-toastr';
import { ConnectComponent } from './connect/connect.component';


@NgModule({
  declarations: [
    LoginComponent,
    SignupComponent,
    ForgetPasswordComponent,
    ChangePasswordComponent,
    ConnectComponent
  ],
  imports: [
    CommonModule,
    AuthRoutingModule,
    MatSelectModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    FormsModule,
    ReactiveFormsModule,
    CoreModule,
    MatSnackBarModule,  
    MatCardModule,  
    ToastrModule.forRoot()
  ],
  exports: [
    LoginComponent,
  ] 
})
export class AuthModule { }
