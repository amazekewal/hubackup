import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { CustomValidators } from '../../shared/custom-validators';
import { ForgotUser } from '../../shared/models/user';
import { MatSnackBar } from '@angular/material';
import { AuthService } from '../../core/services/auth.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit {

  // Forget password form name
  forgetPassword: FormGroup;


  constructor(
    private fb: FormBuilder,
    private matSnack: MatSnackBar,
    private authService: AuthService,
    private router : Router,
  ) { }


  /**
   * Create forget password form
   */
  createForm() {
    this.forgetPassword = this.fb.group({
      email: ['', [Validators.required, CustomValidators.email]],
    });
  }


  /**
   * Forget Password
   */
  onSubmit(params: { valid: boolean, value: ForgotUser }) {
    if (params.valid) {
      this.authService.forgotPassword(params.value)
        .subscribe(res => {
          if (res === true) {
            this.matSnack.open('Email send', 'Close', {
              duration: 3000
            });

            this.router.navigate(['login']);
          } else {
            this.matSnack.open('Email not found', 'Close', {
              duration: 3000
            });
          }
        });
    } else {
      this.matSnack.open('Email not valid', 'Close', {
        duration: 3000
      });
    }
  }


  ngOnInit() {
    this.createForm();
  }

}
