import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { changePwd } from '../../shared/models/user';
import { MatSnackBar } from '@angular/material';
import { AuthService } from '../../core/services/auth.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  // Change password form name
  passwordChange: FormGroup;
  public token;
  public ngPassword: '';
  // Pasword Field Text or password check
  hide = true;


  constructor(
    private fb: FormBuilder,
    private router: Router,
    private matSnack: MatSnackBar,
    private authService: AuthService,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) {

    this.route.params.subscribe(params => {
      this.token = params.token;
    });
  }


  /**
    Create change 
    password form
   */
  createForm() {
    this.passwordChange = this.fb.group({
      password: ['', Validators.required],
      password_confirmation: ['', Validators.required],
      token: this.token,
    })
  }


  /**
   * Change password
   */
  onSubmit(params: { valid: boolean, value: changePwd }) {
    if (params.valid) {
      this.authService.changePassword(params.value)
        .subscribe((res: any) => {
          if (res === true) {
            this.showSuccess();
            this.router.navigate(['login']);
          } else {
            this.matSnack.open('Password Not Change', 'Close', {
              duration: 3000
            });
          }
        });
    }
  }


  /**
    Count capital 
    letters in string
   */
  checkCapitalletter(data) {
    return data.replace(/[^A-Z]/g, "").length;
  }


  /**
    Count numbers 
    in string
   */
  checkNumbers(data) {
    return data.replace(/[^0-9]/g, "").length;
  }


  ngOnInit() {
    this.createForm();

  }


  showSuccess() {
    this.toastr.success('Password Change Sucessfully', 'Thank you');
  }


}
