import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { SignupComponent } from './signup/signup.component';
import { ForgetPasswordComponent } from './forget-password/forget-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { AuthGuard } from '../core/guard/login-guard';
import { ConnectComponent } from './connect/connect.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'connect', component: ConnectComponent },
  { path: '', redirectTo: '/auth/login', pathMatch: 'full' },
  { path: 'signup', component: SignupComponent},
  { path: 'forget-password', component: ForgetPasswordComponent },
  { path: 'reset-password/:token', component: ChangePasswordComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule { }